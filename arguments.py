#!/usr/bin/env python
# -*- coding: utf-8 -*-
# TODO Add file description
"""

"""
__author__ = "Yuriy Petrovskiy"
__copyright__ = "Copyright 2024, mcq-manager"
__credits__ = ["Yuriy Petrovskiy"]
__license__ = ""  # TODO Add licence
__maintainer__ = "Yuriy Petrovskiy"
__email__ = "yuriy.petrovskiy@gmail.com"

import argparse


def parse_arguments():
    parser = argparse.ArgumentParser(description='Sort multiple choice questions.')
    parser.add_argument('file',
                        help='File(s) to process',
                        nargs="+",
                        type=str
                        )
    parser.add_argument('--dry-run',
                        action="store_true",
                        default=False,
                        help='Do not save any files'
                        )
    parser.add_argument('-a', '--append',
                        action="store_true",
                        default=False,
                        help='Append to file if it exists'
                        )
    parser.add_argument('--strict',
                        action="store_true",
                        default=False,
                        help='Strict file check - stop on any error'
                        )
    parser.add_argument('--debug',
                        action="store_true",
                        default=False,
                        help='Enable debug output'
                        )
    parser.add_argument('-s', '--separate-topics',
                        action="store_true",
                        default=False,
                        help='Make separate file for each topic'
                        )
    parser.add_argument('-d', '--duplicates',
                        default="join",
                        help='Duplicate question handling',
                        type=str,
                        choices={
                            'ignore': "Keeps duplicates",
                            'join': "Join topics of all duplicates",
                            'first': "Keep only first duplicate instance and dropping all the rest",
                            'remove': "Remove all instances of questions that have duplicates",
                            'only': "Keeps only questions that have duplicates and joins topics"
                        }
                        )
    parser.add_argument('-l', '--log',
                        default="warning",
                        help='Logging output level',
                        type=str,
                        choices={
                            'error': "Show only errors",
                            'warning': "Show errors and warnings",
                            'info': "",
                            'debug': "Shows debugging information"
                        }
                        )
    parser.add_argument('--log-to-file',
                        action="store_true",
                        default=False,
                        help='Save log to file',
                        )
    parser.add_argument('--log-file-name',
                        default='{dir_name}/{base_file_name}.log',
                        help='Log file name template'
                        )

    parser.add_argument('-f', '--output-format',
                        default=['TBL', 'HRF', 'GIFT'],
                        nargs="+",
                        choices={
                            'TBL': 'Tab separated table',
                            'HRF': 'Human readable format',
                            'GIFT': 'MOODLE GIFT format',
                            'XLSX': 'Microsoft Excel format',
                        },
                        help='Output file format'
                        )
    parser.add_argument('-i', '--input-format',
                        type=str,
                        default='TBL',
                        choices={
                            'TBL': 'Tab separated table',
                            'GIFT': 'MOODLE GIFT format',
                            'XLSX': 'Microsoft Excel format',
                        },
                        )
    parser.add_argument('-o', '--output-file',
                        type=str,
                        default='{dir_name}/{base_file_name}_{format}@{timestamp}.{extension}'
                        )
    parser.add_argument('-t', '--set-topic',
                        type=str,
                        help='Sets topic of all question to given one'
                        )

    return parser.parse_args()
# EOF
