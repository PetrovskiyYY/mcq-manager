#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Converts directory with test files to aggregated TBL format file
"""
__author__ = "Yuriy Petrovskiy"
__copyright__ = "Copyright 2022-2024, mcq-manager"
__credits__ = ["Yuriy Petrovskiy"]
__license__ = "LGPL"
__maintainer__ = "Yuriy Petrovskiy"
__email__ = "yuriy.petrovskiy@gmail.com"

import csv
import os
import sys
from time import strftime

file_name = sys.argv[1]
dir_name = os.path.dirname(file_name)
csv_file = open(file_name, encoding='utf-8')

aggregated_TBL_file = "{dir}/aggregated_TBL@{timestamp}.txt".format(
    dir = dir_name,
    timestamp=strftime("%Y-%m-%d_%H%M%S")
)

csv_file_reader = csv.reader(csv_file, delimiter='\t')

for row in csv_file_reader:
    in_file,topic = row
    print("%s > %s" % (in_file,topic))

    in_file_base = os.path.splitext(os.path.basename(in_file))[0]

    in_file_path = "{dir}\\{file}".format(
        dir = dir_name,
        file = in_file
    )
    cp1251_file_path = "{dir}\\{file}_cp1251.txt".format(
        dir = dir_name,
        file = in_file_base
    )

    # Converting excel file to text (cp1251 encoding)
    cmd = "cscript xlsx2txt.vbs \"{in_file}\" \"{out_file}\"".format(
        in_file = in_file_path,
        out_file = cp1251_file_path
    )
    result = os.system(cmd)
    # print(cmd)
    if result != 0: exit(result)

    utf8_file_path = "{dir}\\{file}_UTF8.txt".format(
        dir=dir_name,
        file=in_file_base
    )

    # Changing encoding to UTF-8
    result = os.system("iconv -f cp1251 -t utf-8 \"{in_file}\" > \"{out_file}\"".format(
        in_file=cp1251_file_path,
        out_file= utf8_file_path
    ))
    if result != 0: exit(result)
    os.system("del \"%s\"" % cp1251_file_path)

    TBL_file_path = "{dir}\\{file}_TBL.txt".format(
        dir=dir_name,
        file=in_file_base
    )

    # Cleaning and sorting
    result = os.system("python sort.py --strict --set-topic=\"{topic}\" --output-format=TBL --output-file=\"{out_file}\" \"{in_file}\"".format(
        topic = topic,
        in_file = utf8_file_path,
        out_file = TBL_file_path
    ))
    if result != 0: exit(result)

    # Same ad previous but with output to aggregated file
    result = os.system(
        "python sort.py --strict --append --set-topic=\"{topic}\" --output-format=TBL --output-file=\"{out_file}\" \"{in_file}\"".format(
            topic=topic,
            in_file=utf8_file_path,
            out_file=aggregated_TBL_file
        ))
    if result != 0: exit(result)

    os.system("del \"%s\"" % utf8_file_path)

result = os.system(
    "python sort.py --strict --duplicates=remove --output-file=\"{out_file}\" \"{in_file}\"".format(
        topic=topic,
        in_file=aggregated_TBL_file,
        out_file="{dir_name}/aggregated_{format}_nodup.txt"
    ))
if result != 0: exit(result)

result = os.system(
    "python sort.py --strict --duplicates=only  --output-format=TBL --output-file=\"{out_file}\" \"{in_file}\"".format(
        topic=topic,
        in_file=aggregated_TBL_file,
        out_file="{dir_name}/aggregated_{format}_duplicates.txt"
    ))
if result != 0: exit(result)

# import subprocess
# subprocess.run(["ls", "-l"])