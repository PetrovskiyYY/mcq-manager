' Script for converting Excel files to TBL format using command line
' Syntax: 
' xlsx2tbl.vbs input_file.xslx output_file.txt
'

Set objArgs = WScript.Arguments
InputName = objArgs(0)
OutputName = objArgs(1)
Set objExcel = CreateObject("Excel.application")
objExcel.application.visible=false
objExcel.application.displayalerts=false
set objExcelBook = objExcel.Workbooks.Open(InputName)
objExcelBook.SaveAs OutputName, -4158 '23
objExcel.Application.Quit
objExcel.Quit