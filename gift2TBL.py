from classes.question_list import QuestionList
import sys
import os
import re
from time import strftime



if '*' in sys.argv[1]:
	for dirname, dirnames, filenames in os.walk('.'):
		# print path to all filenames.
		for filename in filenames:
			fullFileName= os.path.join(dirname, filename)
			if re.match(sys.argv[1],fullFileName):
				print( fullFileName)
				qList = QuestionList()
				qList.import_gift_file(fullFileName)
				qList.export_to_table('%s@%%s.txt' % fullFileName)
else:
	qList = QuestionList()
	qList.import_gift_file(sys.argv[1])
	qList.export_to_table('%s@%s.txt' % (
        sys.argv[1],
        strftime("%Y-%m-%d_%H%M%S")
    ))