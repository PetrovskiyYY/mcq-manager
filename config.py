answerIDLettersEN = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'N']
answerIDLettersRU = ['А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ж', 'З', 'И', 'К', 'Л', 'М']


# Leading content type identification part
LCTIP = {
	"id": ["№","#"],
	"topic": [
		"Тема",
		"Topic",
		"тема",
		"topic",
		"T",
		"Т"  # Cyrillic
	],
	"question": [
		"Текст завдання",
		'Question',
		'Текст задания',
		'текст задания',
		'question',
		'Task',
		'Q',
		"текст завдання",
		"Текст"
	],
	"correctAnswer": [
		"Вірна відповідь",
		"Correct answer",
		'Правильный ответ',
		'правильный ответ',
		"correct answer",
		"+",
		"Правильна відповідь",
		"правильна відповідь",
		"Правильну відповідь",
		"правильну відповідь",
	],
	"answers": answerIDLettersEN + answerIDLettersRU,
	"author": [
		"Автор",
		"Author"
	]
}