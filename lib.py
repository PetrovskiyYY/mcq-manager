import sys
import re
from typing import Dict, Optional


def normalizeText(text):
    text = re.sub('\\s{2,}', ' ', text)  # removing repeating spaces
    text = re.sub('\\s$', '', text)  # removing ending spaces
    text = re.sub('^\\s', '', text)  # removing leading spaces
    return text


##
# @brief String transformation by matrix 
# Replaces in 'text' all occurrences of any key in the given dictionary by its corresponding value.
# Reference: http://markasread.net/post/71884374/python-string-multi-replace-a-php-strtr-like  
# @param text Text where replacements should be made
# @param dic \b Dictionary with key value pairs.
# @return Returns the new string.
#
def strtr(text: str, replacements: Dict[str, str]) -> str:
    # http://code.activestate.com/recipes/81330/
    # Create a regular expression  from the dictionary keys
    regex = re.compile("(%s)" % "|".join(map(re.escape, replacements.keys())))
    # For each match, look-up corresponding value in dictionary
    return regex.sub(lambda mo: str(replacements[mo.string[mo.start():mo.end()]]), text)


##
# @brief escapes GIFT format special symbols
# ~, =, #, {, }, and :
# https://docs.moodle.org/23/en/GIFT_format
#
def escapeGIFT(text):
    return strtr(text, {
        '~': '\\~',
        '=': '\\=',
        '#': '\\#',
        '{': '\\{',
        '}': '\\}',
        ':': '\\:'
    })


def unescapeGIFT(text):
    return strtr(text, {
        '\\~': '~',
        '\\=': '=',
        '\\#': '#',
        '\\{': '{',
        '\\}': '}',
        '\\:': ':'
    })


def uprint(*objects, sep=' ', end='\n', file=sys.stdout):
    enc = file.encoding
    if enc == 'UTF-8':
        print(*objects, sep=sep, end=end, file=file)
    else:
        f = lambda obj: str(obj).encode(enc, errors='backslashreplace').decode(enc)
        print(*map(f, objects), sep=sep, end=end, file=file)


# Regular expression to match positive number with fractional part
NUMBER_RE = re.compile("^\\d+?\\.\\d+?$")


def is_number(obj) -> bool:
    if isinstance(obj, (int, float)):
        return True
    if isinstance(obj, str) and NUMBER_RE.match(obj):
        return True
    return False


def as_float(obj) -> Optional[float]:
    if isinstance(obj, (int, float)):
        return float(obj)
    if isinstance(str) and NUMBER_RE.match(obj):
        return float(obj)
    return False


NON_TEXT_SYMBOLS = " \t.,-/\\;:!?_=-+(){}[]~`'\"%^&*@#№<>\r\n"


def has_text(string) -> bool:
    """
    Determines if string contains any text
    :param string:
    :return: True if str contains at least one symbol not from NON_TEXT_SYMBOLS list
    """
    if isinstance(string, str) and len(string.strip(NON_TEXT_SYMBOLS)) > 0:
        return True
    return False
