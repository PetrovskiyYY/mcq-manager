#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Converts directory with test files to aggregated TBL format file
"""
__author__ = "Yuriy Petrovskiy"
__copyright__ = "Copyright 2022-2024, mcq-manager"
__credits__ = ["Yuriy Petrovskiy"]
__license__ = "LGPL"
__maintainer__ = "Yuriy Petrovskiy"
__email__ = "yuriy.petrovskiy@gmail.com"

import os
from time import strftime
import logging

LOG_FILE_MAX_BYTES = 1024 * 1024


def setup_logger(args):
    """Setup and configure the logger based on the provided arguments."""
    log_level = getattr(logging, args.log.upper(), None)
    if not isinstance(log_level, int):
        raise ValueError(f"Invalid log level: {args.log}")

    logger = logging.getLogger('root')
    logger.setLevel(log_level)
    formatter = logging.Formatter('%(asctime)s [%(levelname)s]: %(message)s')

    if args.log_to_file:
        log_file_path = args.log_file_name.format(
            dir_name=os.path.dirname(args.file[0]),
            base_file_name=strftime("%Y-%m-%d_%H%M%S")
        )
        log_file_dir = os.path.abspath(os.path.dirname(log_file_path))

        if not os.path.isdir(log_file_dir):
            os.makedirs(log_file_dir)

        file_handler = RotatingFileHandler(log_file_path, maxBytes=LOG_FILE_MAX_BYTES, backupCount=1)
        file_handler.setFormatter(formatter)
        file_handler.setLevel(log_level)
        logger.addHandler(file_handler)

    console_handler = logging.StreamHandler()
    console_handler.setLevel(log_level)
    console_handler.setFormatter(formatter)
    logger.addHandler(console_handler)
    return logger
