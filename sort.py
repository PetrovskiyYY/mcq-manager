# -*- coding: utf-8 -*-
from pydantic.v1 import parse_file_as

from arguments import parse_arguments
from lib import uprint
from classes.question_list import QuestionList

import os

from logger import setup_logger

DEFAULT_DIR = "."


def determine_dir_name(file_path):
    """Determine the directory name or use a default if not specified."""
    directory = os.path.dirname(file_path)
    return directory if directory and not directory.isspace() else DEFAULT_DIR


def parse_file(file_path, args, logger):
    """Parse the input file based on format and log any errors."""
    questions = QuestionList()
    try:
        if args.input_format == 'TBL':
            questions.import_table_file(file_path, strict=args.strict, debug=args.debug)
        elif args.input_format == 'GIFT':
            questions.import_gift_file(file_path, strict=args.strict, debug=args.debug)
        elif args.input_format == 'XLSX':
            questions.import_xlsx_file(file_path, strict=args.strict, debug=args.debug)
        else:
            logger.error(f"Unexpected input file format: {args.input_format}")
            exit(1)
        logger.info(f"Parsed {len(questions.data)} questions.")
    except FileNotFoundError:
        logger.error(f"Input file not found: {file_path}")
        exit(1)
    return questions


def handle_duplicates(questions, args):
    """Handle duplicate questions according to the specified strategy."""
    if args.duplicates == 'join':
        questions.collapse_duplicate_topics()
    elif args.duplicates == 'remove':
        questions.remove_duplicates()
    elif args.duplicates == 'first':
        questions.keep_first_duplicate()
    elif args.duplicates == 'only':
        questions.keep_only_duplicated()
    elif args.duplicates != 'ignore':
        raise ValueError(f"Unexpected duplication handling `{args.duplicates}`.")
    print(f"{len(questions.data)} questions after duplicate handling.")


def save_output(questions, args, base_file_name, dir_name):
    """Separate questions by topic and save to files if required."""
    topics = questions.separateTopics()
    for topic, topic_questions in topics.items():
        uprint(f'{len(topic_questions)} questions with topic "{topic}"')
    if args.separate_topics:
        for topic, topic_questions in topics.items():
            topic_list = QuestionList()
            topic_list.data = topic_questions
            if not args.dry_run:
                topic_file_name = f"{base_file_name}_{topic}"
                topic_list.generate_output(
                    args=args, base_file_name=topic_file_name, dir_name=dir_name
                )
            else:
                print("Read only mode - results not saved")
    else:
        if args.set_topic:
            questions.resetTopic(args.set_topic)
        if not args.dry_run:
            questions.generate_output(
                args=args, base_file_name=base_file_name, dir_name=dir_name
            )
        else:
            print("Read only mode - results not saved")


def process_question_file(file_path, args, logger):
    """
    Processes a single file: parses it, handles duplicates, and saves the output.
    """
    base_file_name = os.path.splitext(os.path.basename(file_path))[0]  # Extract file name without extension
    output_dir = determine_dir_name(file_path)  # Determine directory from file path
    questions = parse_file(file_path, args, logger)  # Parse the file into questions
    handle_duplicates(questions, args)  # Handle duplicate questions
    save_output(questions, args, base_file_name, output_dir)  # Save to output


# Main processing loop
args = parse_arguments()
logger = setup_logger(args)

for file_path in args.file:
    process_question_file(file_path, args, logger)