# Multiple Сhoice Question manager
Package for managing simple multiple choice questions (MCQs).

#Requirements:

- All questions should be in UTF-8 encoding
- Questions should conform to format requirements

#Question format requirements

1. Each line consists of leading content type identification part and contentseparated by tabulation character (\t).

```
№	...
(Тема|Topic)	....
(Текст задания|текст задания|Question|question)	....
(Правильный ответ|)	....
B	....
C	....
D	....
E	....
...	....
```
for example:
```
#	krok  2017
Topic	Classification and mechanism of action of enzymes
Question	Protective function of saliva is based on several mechanisms, including the presence of enzyme that has bactericidal action and causes lysis of complex capsular polysaccharides of staphylococci and streptococci. Name this enzyme:
Correct answer	Lysozyme
B	Alpha-amylase
C	Oligo-1,6-glucosidase
D	Collagenase
E	Beta-glucuronidase

```

2. Following 

# Features
Software has the following features:

- Question sorting
- Duplicate extraction
- Duplicate joining
- Separation of questions by topics


# Command line parameters
- --dry-run - Do not save any files // не сохранять файлы (полезно для проверки формата файла)
- -a --append - Append to file if it exists
- --strict - Strict file check
- -s --separate-topics - Make separate file for each topic
- -d --duplicates - Duplicate question handling
	- join - Join topics of all duplicates
	- first - Keep only first duplicate instance and dropping all the rest
	- remove - Remove all instances of questions that have duplicates
	- only - Keeps only questions that have duplicates and joins topics
	- ignore - 
- -l --log - Logging output level
	- error - Show only errors",
	- warning - Show errors and warnings",
	- info - 
	- debug - Shows debugging information
- --log-to-file - Save log to file
- --log-file-name <file> - Log file name template
- -f --output-format - Output file format
		'TBL':'Tab separated table',
		'HRF':'Human readable format',
		'GIFT':'MOODLE GIFT format'
- -o --output-file <file>
- -t','--set-topic <topic> - Sets topic of all question to given one


##Examples:
Common use: sorts questions alphabeticaly, joins duplicates
```
python sort.py "questions.txt"
```

Duplicates extraction:
```
python sort.py "questions.txt" --keepOnlyDuplicated
```

Separates topics, keeps duplicates and outputs only TBL
```
python sort.py "questions.txt" -f TBL -s -d ignore
```


```
python sort.py "questions.txt" -f TBL -d remove -t "Патологічна_фізіологія"
```

Converts Questions from GIFT format (https://docs.moodle.org/23/en/GIFT_format) to TBL:
```
python gift2TBL.py "questions.txt"
```

Converts files according to file list
```
python dir_converter.py "files.txt"
```

# Частые ошибки

- Пустая тема
- Нужно убрать пустой вариант или ввести значение "-" если Вам важно сохранить форму с 5-ю ответами (этот вариант хуже)
- Нет темы. (это обязательный компонент и его отсутствие замедляет импорт и обработку данных)
- Смещение граф
