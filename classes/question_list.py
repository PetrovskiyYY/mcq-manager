#!/usr/bin/env python
# -*- coding: utf-8 -*-
# TODO Add file description
"""

"""
__author__ = "Yuriy Petrovskiy"
__copyright__ = "Copyright 2022, mcq-manager"
__credits__ = ["Yuriy Petrovskiy"]
__license__ = ""  # TODO Add licence
__version__ = "0.0.0"
__maintainer__ = "Yuriy Petrovskiy"
__email__ = "yuriy.petrovskiy@gmail.com"
__status__ = "dev"

import logging
import re
from pprint import pprint
from time import strftime
from typing import List, Optional

from pydantic import BaseModel, Field

from classes.question import Question, QuestionType
from classes.question_answer import QuestionAnswer
from config import LCTIP
from lib import escapeGIFT, uprint

excel_output_formats = {
    'percent': {
        'num_format': '0.00%;[red]-0.00%',
        # 'align': 'left',
    },
}


class QuestionList(BaseModel):
    data: List[Question] = Field(default_factory=list)

    def __str__(self):
        result = ''
        for (question_index, question) in enumerate(self.data):
            result += f"{question_index}: {question}"
        return result

    def add(self, question_data, debug: bool = False) -> Optional[Question]:
        new_question = Question(question_data=question_data)
        if not new_question.empty:
            self.data.append(new_question)
            if debug:
                print(new_question)
            return new_question

    def keep_first_duplicate(self):
        sortedData = sorted(self.data, key=lambda q: q.text)  # sort by text
        newData = []
        previousItem = None
        for item in sortedData:
            if item != previousItem:
                newData.append(item)
                previousItem = item
        self.data = newData

    def collapse_duplicates(self):
        #  Sorting data
        sortedData = sorted(self.data, key=lambda q: q.text)  # sort by text
        newData = []  # Creating new list
        for question in sortedData:
            if len(newData) > 0:  # Checking it is not the first element
                if question != newData[-1]:
                    # Question is different
                    newData.append(question)
                else:
                    # Adding topic of duplicated question to first instance
                    if isinstance(newData[-1].topic, list):
                        # Appending to list if it is 2+ occurrence of this question
                        newData[-1].topic.append(question.topic)
                    else:
                        # it is a duplicate. Creating new list
                        combined_topic = [newData[-1].topic, question.topic]
                        newData[-1].set_topic(combined_topic)
            else:
                # It is the first element in list
                newData.append(question)  # Adding to new list

        self.data = newData

    def remove_duplicates(self):
        self.collapse_duplicates()
        newData = []
        # Removing all items that have duplicates
        for question in self.data:
            if not isinstance(question.topic, list):
                newData.append(question)
        self.data = newData

    def keep_only_duplicated(self):
        self.collapse_duplicates()
        newData = []
        # Removing all items that have duplicates
        for question in self.data:
            if isinstance(question.topic, list):
                topic_string = ", ".join(question.topic)
                question.set_topic(topic_string)
                newData.append(question)
        self.data = newData

    ##
    # @brief Removes duplicated items and joins their topics
    # @return None
    #
    def collapse_duplicate_topics(self):
        #  Sorting data
        sorted_data = sorted(self.data, key=lambda q: q.text if q.text is not None else str(q.text))  # sort by text
        newData = []  # Creating new list
        for question in sorted_data:
            if len(newData) > 0:  # Checking it is not the first element
                if question != newData[-1]:
                    # Question is different
                    newData.append(question)
                else:
                    # Adding topic of duplicated question to first instance
                    new_topic = newData[-1].topic + ", " + question.topic
                    newData[-1].set_topic(new_topic)
            else:
                # It is the first element in list
                newData.append(question)  # Adding to new list
        self.data = newData

    ##
    # @brief separates items by topic
    # @return dict with topics as keys
    #
    def separateTopics(self):
        sortedData = sorted(self.data, key=lambda q: q.text)  # sort by text
        topics = {}  # Creating dictionary of topics
        for question in sortedData:
            if question.topic in topics.keys():
                # Topic already exist
                topics[question.topic].append(question)  # Adding to new list
            else:
                topics[question.topic] = [question]
        return topics

    def resetTopic(self, topic):
        """
        Resets question topic for all questions in list
        :param topic: Topic to set for all questions in list
        :return: None
        """
        for question in self.data:
            question.set_topic(topic)

    # https://docs.moodle.org/23/en/GIFT_format
    # Here are some common GIFT symbols and their use.
    # Symbols	Use
    # // text	Comment until end of line (optional)
    # ::title::	Question title (optional)
    # text	Question text (becomes title if no title specified)
    # [...format...]	The format of the following bit of text. Options are [html], [moodle], [plain] and [markdown].
    #                   The default is [moodle] for the question text, other parts of the question default to the format
    #                   used for the question text.
    # {	Start answer(s) -- without any answers, text is a description of following questions
    # {T} or {F}	True or False answer; also {TRUE} and {FALSE}
    # { ... =right ... }	Correct answer for multiple choice, (multiple answer? -- see page comments)
    #                       or fill-in-the-blank
    # { ... ~wrong ... }	Incorrect answer for multiple choice or multiple answer
    # { ... =item -> match ... }	Answer for matching questions
    # #feedback text	Answer feedback for preceding multiple, fill-in-the-blank, or numeric answers
    # {#	Start numeric answer(s)
    # answer:tolerance	Numeric answer accepted within ± tolerance range
    # low..high	Lower and upper range values of accepted numeric answer
    # =%n%answer:tolerance	n percent credit for one of multiple numeric ranges within tolerance from answer
    # }	End answer(s)
    # \character	Backslash escapes the special meaning of ~, =, #, {, }, and :
    # \n	Places a newline in question text -- blank lines delimit questions

    def export_to_gift(self, fileName, encoding='UTF-8', append=False):
        mode = 'a' if append else 'w'
        logging.info("Saving file '%s'.", fileName)
        outFile = open(fileName, mode, encoding=encoding)
        questionID = 0
        for question in self.data:
            questionID += 1
            outFile.write('// #%d\n' % questionID)

            if question.author is not None:
                outFile.write(f"// Author: {question.author}\n")

            if question.topic is not None:
                outFile.write(f'\n$CATEGORY: {escapeGIFT(question.topic)}\n\n')

            outFile.write('%s\n' % escapeGIFT(question.text))
            outFile.write('{\n')

            question_type = question.type
            if question_type in [QuestionType.ONE_ANSWER, QuestionType.CORRECT_AND_SEVERAL_POSITIVE_ANSWERS]:
                for answer in question.answers:
                    if answer.value == 100:
                        outFile.write('=')
                    elif answer.value == 0:
                        outFile.write('~')
                    else:
                        outFile.write(f"=%{answer.value}%")
                    outFile.write('%s\n' % escapeGIFT(answer.text))
            elif question_type == QuestionType.MULTIPLE_ANSWERS:
                for answer in question.answers:
                    outFile.write(f"~%{answer.value}%")
                    outFile.write('%s\n' % escapeGIFT(answer.text))
            else:
                logging.error(f"Unknown question type: {question_type}")

            outFile.write('}\n\n')
        outFile.close()

    def export_to_table(self, file_name, encoding='UTF-8', append=False):
        mode = 'a' if append else 'w'
        logging.info("Saving file '%s'.", file_name)
        outFile = open(file_name, mode, encoding=encoding)
        questionID = 0
        for question in self.data:
            questionID += 1
            outFile.write('%s\t%d\n' % (LCTIP['id'][0], questionID))
            outFile.write('%s\t%s\n' % (LCTIP['topic'][0], question.topic))
            outFile.write('%s\t%s\n' % (LCTIP['question'][0], question.text))
            answerID = 0
            for answer in question.answers:
                if answer.is_correct:
                    outFile.write('%s\t' % LCTIP['correctAnswer'][0])
                else:
                    outFile.write('%s\t' % LCTIP['answers'][answerID])
                outFile.write('%s\n' % answer.text)
                answerID += 1
            if question.author_is_defined:
                outFile.write('%s\t%s\n' % (LCTIP['author'][0], question.author))
        outFile.close()

    def exportToHRF(self, fileName, encoding='UTF-8', append=False):
        """
        Export to human-readable format.
        :param fileName:
        :param encoding:
        :param append:
        :return:
        """
        mode = 'a' if append else 'w'
        logging.info("Saving file '%s'.", fileName)
        outFile = open(fileName, mode, encoding=encoding)
        questionID = 0
        for question in self.data:
            questionID += 1
            outFile.write('%d. ' % questionID)
            outFile.write('(%s) ' % question.topic)
            outFile.write('%s\n' % question.text)
            for answer in question.answers:
                if answer.is_correct:
                    outFile.write('+')
                elif answer.is_incorrect:
                    outFile.write('-')
                else:
                    outFile.write('~')
                outFile.write('%s\n' % answer.text)
            outFile.write('\n')
        outFile.close()

    def excel_formats(self, workbook):
        result = {}
        for name, definition in excel_output_formats.items():
            result[name] = workbook.add_format(definition)
        return result

    def export_to_xlsx(self, fileName, encoding='UTF-8', append=False):
        import xlsxwriter

        logging.info("Saving file '%s'.", fileName)
        workbook = xlsxwriter.Workbook(fileName, {'remove_timezone': True})
        worksheet = workbook.add_worksheet('test')
        formats = self.excel_formats(workbook=workbook)
        row = 0
        col = 0
        questionID = 0

        for question in self.data:
            questionID += 1
            question_type = question.type

            # Question ID
            worksheet.write_string(row=row, col=col, string=LCTIP['id'][0])
            col += 1
            worksheet.write_string(row=row, col=col, string=str(questionID))
            col = 0
            row += 1

            # Topic
            worksheet.write_string(row=row, col=col, string=LCTIP['topic'][0])
            col += 1
            worksheet.write_string(row=row, col=col, string=question.topic)
            col = 0
            row += 1

            # Text
            worksheet.write_string(row=row, col=col, string=LCTIP['question'][0])
            col += 1
            worksheet.write_string(row=row, col=col, string=question.text)
            col = 0
            row += 1

            # Answers
            answer_id = 0
            for answer in question.answers:
                if question_type == QuestionType.ONE_ANSWER:
                    if answer.is_correct:
                        worksheet.write_string(row=row, col=col, string=LCTIP['correctAnswer'][0])
                    else:
                        worksheet.write_string(row=row, col=col, string=LCTIP['answers'][answer_id])
                    col += 1
                    worksheet.write_string(row=row, col=col, string=answer.text)
                    col = 0
                    row += 1
                    answer_id += 1
                elif question_type in [QuestionType.MULTIPLE_ANSWERS, QuestionType.CORRECT_AND_SEVERAL_POSITIVE_ANSWERS]:
                    worksheet.write_number(row=row, col=col, number=answer.value / 100, cell_format=formats['percent'])
                    col += 1
                    worksheet.write_string(row=row, col=col, string=answer.text)
                    col = 0
                    row += 1
                    answer_id += 1
                else:
                    logging.error(f"Unknown question type: {question_type}")

            # Author
            if question.author_is_defined:
                worksheet.write_string(row=row, col=col, string=LCTIP['author'][0])
                col += 1
                worksheet.write_string(row=row, col=col, string=question.author)
                col = 0
                row += 1
        workbook.close()

    def import_table_file(self, fileName, debug=False, strict=False):
        """
        Imports table file
        :param fileName: File name
        :param debug: Debug output flag
        :param strict: Strict analyze flag
        :return: None
        """
        infile = open(fileName, 'r', encoding='UTF-8')
        current_question_data = []
        line_number = 0
        errors = {}
        current_topic = None

        empty_string_re = re.compile("^\\s*$")

        for (line_id, line) in enumerate(infile):
            if empty_string_re.match(line):
                # Skipping empty strings
                continue

            string = line.strip()  # removing leading and trailing spaces
            line_number += 1
            string_parts = string.split("\t")  # Breaking line to field name and value

            # TODO: Maybe change "-" to none
            if len(string_parts) == 1:
                if string_parts[0] in LCTIP['topic']:
                    string_parts.append("-")  # no topic
                elif string_parts[0] in LCTIP['id']:
                    string_parts.append("-")  # no ID

            if len(string_parts) != 2:
                uprint("Line %d, Question %d: %d component(s) found in line (need exactly 2): \"%s\"." % (
                    line_number,
                    len(self.data) + 1,  # question id
                    len(string_parts),
                    string
                ))
                errors.setdefault(len(self.data) + 1, []).append(
                    "Line %d, Question %d: %d component(s) found in line (need exactly 2)." % (
                        line_number,
                        len(self.data) + 1,
                        len(string_parts)
                    ))
                continue
            else:
                line_name, line_data = string_parts

                if line_name in LCTIP["id"]:
                    # Skipping line if it is question ID
                    continue

                elif line_name in LCTIP['question']:
                    # Finish previous question and start new one
                    question_object = self.add(current_question_data)
                    if question_object:
                        validation_result = question_object.validate_data()
                        if len(validation_result) > 0:
                            # Question validation failed
                            validation_result.append(question_object)
                            errors.setdefault(len(self.data) + 1, []).append(validation_result)
                            if debug:
                                print(f"Error: {validation_result}")

                    # Resetting question data
                    current_question_data = [
                        {
                            'name': line_name,
                            'value': line_data
                        },
                        {
                            'name': LCTIP['topic'][0],
                            'value': current_topic
                        },
                    ]
                    if debug:
                        print("%d: Question %d" % (line_number, len(self.data) + 1))
                elif line_name in LCTIP['topic']:
                    current_topic = line_data
                else:
                    # There is no such section
                    current_question_data.append({
                        'name': line_name,
                        'value': line_data
                    })

        # Adding last question
        question_object = self.add(current_question_data)
        if debug:
            print("%d: Question %d" % (line_number, len(self.data) + 1))
        validation_result = question_object.validate_data()
        if len(validation_result) > 0:
            # Question validation failed
            validation_result.append(question_object)
            errors.setdefault(len(self.data) + 1, []).append(validation_result)

        if strict and len(errors) > 0:
            pprint(errors)
            exit(1)

    def import_xlsx_file(self, fileName, debug=False, strict=False):
        import openpyxl
        workbook = openpyxl.load_workbook(fileName)
        sheet = workbook.active

        current_question_data = []
        line_number = 0
        errors = {}

        for (index, row) in enumerate(sheet.iter_rows()):
            line_number = index + 1
            line_name = row[0].value
            line_data = row[1].value
            if debug:
                print(f"{line_number}: {line_name} {line_data}|{len(row)}")

            if line_name is None:
                # Skipping line without value in the first column
                continue

            elif line_name in LCTIP["id"]:
                # Skipping line if it is question ID
                continue

                # if stringName in currentQuestion:
            elif line_name in LCTIP['question']:
                # Finish previous question and start new one
                question_object = self.add(current_question_data, debug=debug)
                if question_object:
                    validation_result = question_object.validate_data()
                    if len(validation_result) > 0:
                        # Question validation failed
                        validation_result.append(question_object)
                        errors.setdefault(len(self.data) + 1, []).append(validation_result)

                # Resetting question data
                current_question_data = [
                    {
                        'name': line_name,
                        'value': line_data
                    },
                    {
                        'name': LCTIP['topic'][0],
                        'value': current_topic
                    },
                ]
                if debug:
                    print("%d: Question %d" % (line_number, len(self.data) + 1))

            elif line_name in LCTIP['topic']:
                current_topic = line_data
            else:
                # There is no such section
                current_question_data.append({
                    'name': line_name,
                    'value': line_data
                })

        # Adding last question
        question_object = self.add(current_question_data, debug=debug)
        if question_object is not None:
            # raise ValueError(f"Got empty result while parsing line {line_number}: {current_question_data}")
            validation_result = question_object.validate_data()
            if len(validation_result) > 0:
                # Question validation failed
                validation_result.append(question_object)
                errors.setdefault(len(self.data) + 1, []).append(validation_result)

            if strict and len(errors) > 0:
                pprint(errors)
                exit(1)

    def import_gift_file(self, fileName, debug=False, strict=False):
        import re
        from lib import unescapeGIFT

        infile = open(fileName, 'r', encoding='UTF-8')
        currentQuestion = {}
        line_number = 0

        currentCategory = None
        currentQuestion = None

        commentMatch = re.compile("^// ")
        emptyStringMatch = re.compile("^\\s*$")
        endOfQuestionMatch = re.compile("^}$")
        categoryMatch = re.compile("^\\$CATEGORY:\\s*(.+)$")
        textMatch = re.compile("^::(.+)::(.+){$")
        correctAnswerMatch = re.compile("^\\t*=(.+)$")
        incorrectAnswerMatch = re.compile("^\\t*~(.+)$")

        for line in infile:
            string = line.strip()
            line_number += 1

            if commentMatch.match(string):
                continue
            elif emptyStringMatch.match(string):
                continue
            elif categoryMatch.match(string):
                currentCategory = categoryMatch.match(string).group(1)

            elif endOfQuestionMatch.match(string):  # End of question
                self.data.append(currentQuestion)
                currentQuestion = None
            elif textMatch.match(string):
                currentQuestion = Question()
                currentQuestion.text = unescapeGIFT(textMatch.match(string).group(2))
                currentQuestion.set_topic(currentCategory)
            elif correctAnswerMatch.match(string):
                if currentQuestion:
                    new_answer = QuestionAnswer(
                        text=unescapeGIFT(correctAnswerMatch.match(string).group(1)),
                        value=100
                    )
                    currentQuestion.answers.append(new_answer)
                else:
                    logging.error("Unable to add correct answer without question (line %s): %s", line_number, line)
            elif incorrectAnswerMatch.match(string):
                if currentQuestion:
                    new_answer = QuestionAnswer(
                        text=unescapeGIFT(incorrectAnswerMatch.match(string).group(1)),
                        value=0
                    )
                    currentQuestion.answers.append(new_answer)
                else:
                    logging.error("Unable to add incorrect answer without question (line %s): %s", line_number, line)
            else:
                # Question text without marker
                currentQuestion = Question()
                currentQuestion.text = unescapeGIFT(line)
                currentQuestion.set_topic(currentCategory)

    def generate_output(
        self,
        args,
        base_file_name: str,
        dir_name: str,
    ):
        if 'GIFT' in args.output_format:
            self.export_to_gift(args.output_file.format(
                dir_name=dir_name,
                base_file_name=base_file_name,
                format='GIFT',
                timestamp=strftime("%Y-%m-%d_%H%M%S"),
                extension='txt',
            ), append=args.append)
        if 'HRF' in args.output_format:
            self.exportToHRF(args.output_file.format(
                dir_name=dir_name,
                base_file_name=base_file_name,
                format='HRF',
                extension='txt',
                timestamp=strftime("%Y-%m-%d_%H%M%S")
            ), append=args.append)
        if 'TBL' in args.output_format:
            self.export_to_table(args.output_file.format(
                dir_name=dir_name,
                base_file_name=base_file_name,
                format='TBL',
                extension='txt',
                timestamp=strftime("%Y-%m-%d_%H%M%S")
            ), append=args.append)
        if 'XLSX' in args.output_format:
            self.export_to_xlsx(args.output_file.format(
                dir_name=dir_name,
                base_file_name=base_file_name,
                format='XLSX',
                extension='xlsx',
                timestamp=strftime("%Y-%m-%d_%H%M%S")
            ), append=args.append)
# EOF
