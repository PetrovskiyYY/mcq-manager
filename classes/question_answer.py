from pydantic import BaseModel


class QuestionAnswer(BaseModel):
    value: float
    text: str

    @property
    def is_correct(self):
        if self.value == 100:
            return True
        else:
            return False

    @property
    def is_incorrect(self):
        if self.value <= 0:
            return True
        else:
            return False
