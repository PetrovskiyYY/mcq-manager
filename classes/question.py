import logging
import re
from enum import Enum, auto
from typing import Optional, List

from pydantic import BaseModel, PrivateAttr

from classes.question_answer import QuestionAnswer
from config import LCTIP
from lib import normalizeText, strtr, is_number, as_float, has_text

percent_re = re.compile("^\s*(-?[0-9]{1,3}([\.,][0-9]{1,5})?)%\s*$")


class QuestionType(Enum):
    UNKNOWN = auto()
    ONE_ANSWER = auto()  # MCQ with only one correct answer
    CORRECT_AND_SEVERAL_POSITIVE_ANSWERS = auto()  # MCQ with one correct and several positive answers
    MULTIPLE_ANSWERS = auto()  # MCQ with multiple answers


class Question(BaseModel):
    text: Optional[str] = None
    author: Optional[str] = None
    _topic: Optional[str] = PrivateAttr(None)
    answers: List[QuestionAnswer] = []

    def __init__(self, question_data=None, **kwargs):
        super().__init__(**kwargs)
        if question_data:
            self.import_data(question_data)

    def __eq__(self, other):
        if other.__class__ != self.__class__: 
            return False
        elif self.text != other.text: 
            return False
        elif self.answers != other.answers: 
            return False
        else:
            return True

    def __str__(self):
        question_data = {
            'topic': self.topic,
            'text': self.text,
            'answers': self.answers,
            'author': self.author,
            'type': self.type
        }
        return f'<Question {question_data}>'

    def __repr__(self):
        return self.__str__()

    @property
    def topic(self):
        return self._topic

    def set_topic(self, value):
        if len(value) > 255:
            raise ValueError(f"Topic is too long: `{value}`")
        self._topic = value

    # def check_topic(self, value):
    #     raise ValueError(f"Topic is too long: `{value}`")

    @property
    def empty(self):
        if self.text and len(self.text) > 0:
            return False
        elif isinstance(self.answers, list) and len(self.answers) > 0:
            return False
        else:
            return True

    def import_data(self, data):
        for record in data:
            if record['name'] in LCTIP['topic']:
                self.set_topic(normalizeText(record['value']))
            elif record['name'] in LCTIP['question']:
                self.text = normalizeText(record['value'])
            elif record['name'] in LCTIP['correctAnswer']:
                self.answers.append(QuestionAnswer(
                    value=100,
                    text=normalizeText(record['value'])
                ))
            elif record['name'] in LCTIP['answers']:
                self.answers.append(QuestionAnswer(
                    value=0,
                    text=normalizeText(record['value'])
                ))
            elif record['name'] in LCTIP['author']:
                self.author = normalizeText(record['value'])
            elif is_number(record['name']):
                value = as_float(record['name'])
                if -1 <= value <= 1:
                    value = value * 100

                # print(f"{record['name']} > {value}")
                self.answers.append(QuestionAnswer(
                    value=value,
                    text=normalizeText(record['value'])
                ))
            else:
                # print(f"{record['name'].__class__.name}:{record['name']}")
                percent_match = percent_re.match(record['name'])
                if percent_match:
                    try:
                        value = float(strtr(text=percent_match.group(1), replacements={',': '.'}))
                    except Exception as e:
                        logging.error(f"Failed to covert percent field name `{record['name']}`: {str(e)}")
                        continue

                    self.answers.append(QuestionAnswer(
                        value=value,
                        text=normalizeText(record['value'])
                    ))
                else:
                    print("Incorrect field name: %s" % record['name'])

    def validate_data(self):
        result = []
        if not self.text or len(self.text) == 0:
            result.append("Question has no text.")
        elif len(self.text) < 8:
            result.append("Question text is too short.")

        if len(self.answers) == 0:
            result.append("Question has no answers.")
        elif len(self.answers) == 1:
            result.append("Question has only one answer.")
        else:
            correct_answers = 0
            for answer in self.answers:
                if answer.value is None:
                    result.append("Answer has no 'value' component.")
                elif not isinstance(answer.value, float) or answer.value < -100 or answer.value > 100:
                    result.append("Answer 'value' component has incorrect content (%s)." % answer)
                elif answer.value == 100:
                    correct_answers += 1

            # if correct_answers == 0:
            #     result.append("Question has no correct answer.")
            # el
            if correct_answers > 1:
                result.append("Question has more, then one correct answer (%d)." % correct_answers)

        return result

    def valid(self, debug=False):
        result = self.validate_data()
        if len(result) > 0:
            if debug:
                print(result)
            return False
        else:
            return True

    @property
    def is_mcq_with_one_answer(self):
        return self.correct_answer_count == 1 and self.positive_answer_count == 1 and self.negative_answer_count == 0

    @property
    def is_mcq_with_several_positive_answers(self):
        return self.correct_answer_count == 1 and self.positive_answer_count > 1

    @property
    def is_mcq_with_multiple_answers(self):
        return 99 < self.answer_value_sum < 101 and self.positive_answer_count + self.negative_answer_count > 1

    @property
    def type(self) -> QuestionType:
        if self.is_mcq_with_one_answer:
            return QuestionType.ONE_ANSWER
        elif self.is_mcq_with_several_positive_answers:
            return QuestionType.CORRECT_AND_SEVERAL_POSITIVE_ANSWERS
        elif self.is_mcq_with_multiple_answers:
            return QuestionType.MULTIPLE_ANSWERS
        else:
            logging.error(f"Unknown question type: {self.answers}")
            return QuestionType.UNKNOWN
            # raise ValueError(f"Unknown question type: {self}")

    @property
    def correct_answer_count(self):
        result = 0
        for answer in self.answers:
            if answer.value == 100:
                result += 1
        return result

    @property
    def positive_answer_count(self):
        result = 0
        for answer in self.answers:
            if answer.value > 0:
                result += 1
        return result

    @property
    def negative_answer_count(self):
        result = 0
        for answer in self.answers:
            if answer.value < 0:
                result += 1
        return result

    @property
    def answer_value_sum(self):
        result = 0
        for answer in self.answers:
            if answer.value > 0:
                result += answer.value
        return result

    @property
    def author_is_defined(self):
        return has_text(self.author)
